cmake_minimum_required(VERSION 3.5)
project(mp2-lab7-texts)

# Include directory with public headers
set (INCLUDE "./include")
include_directories(${INCLUDE})

# Add Google Test
include_directories("./gtest")
set(LIBRARY_DEPS "gtest")


# Add all submodules
add_subdirectory(src)
add_subdirectory(test)
add_subdirectory(gtest)
add_subdirectory(viewer)