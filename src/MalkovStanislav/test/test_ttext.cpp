#include "../include/TText.h"

#include <gtest\gtest.h>

TEST(TText, CanCreate) {
    TText* t;
    EXPECT_NO_THROW(t = new TText());
}

TEST(TText, CanReset) {
    TText t(new TTextLink("z"));
    EXPECT_FALSE(t.Reset());
}

TEST(TText, CanGetLink) {
    TText t(new TTextLink("z"));
    t.Reset();
    EXPECT_FALSE(std::strcmp(*(t.GetCurrentLink()->GetStr()), "z"));
}

TEST(TText, CanGetLineWhenEmpty) {
    TText text(new TTextLink());
    EXPECT_EQ(text.GetLine().length(), 0);
}

TEST(TText, CanCopy) {
    TText t(new TTextLink("z"));
    TText t2(t);
    t2.Reset();
    EXPECT_FALSE(std::strcmp(*(t2.GetCurrentLink()->GetStr()), "z"));
}

TEST(TText, CanCheckEmpty) {
    TText t;
    t.Reset();
    EXPECT_TRUE(t.IsTextEnded());
}

TEST(TText, CanGoNext) {
    TText t(new TTextLink("A"));
    t.InsNextLine("B");
    t.Reset();
    t.GoNext();
    EXPECT_FALSE(std::strcmp(t.GetLine().c_str(), "B"));
}

TEST(TText, CanGoPrev) {
    TText t(new TTextLink("A"));
    t.InsNextLine("B");
    t.Reset();
    t.GoNextLink();
    t.GoPrevLink();
    EXPECT_FALSE(std::strcmp(t.GetLine().c_str(), "A"));
}

TEST(TText, CantGoPrevWhenPathIsEmpty) {
    TText t;
    EXPECT_EQ(t.GoPrevLink(), MSG_PATH_IS_EMPTY);
}