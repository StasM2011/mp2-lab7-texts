#include <iostream>
#include <gtest\gtest.h>
#include "../include/TText.h"

int main(int argc, char** argv) {
    TTextLink::InitMemSystem();

    ::testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();

    std::cout << "\nLoading file example: \n";
    TText text;
    text.Read("file.txt");
    text.Print();
    getchar();
    return 0;
}